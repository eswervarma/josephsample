﻿import {Injectable, Inject} from '@angular/core';
import {Http, URLSearchParams} from '@angular/http';
import {APP_BASE_HREF} from '@angular/common';
import {ORIGIN_URL} from './constants/baseurl.constants';
import {IUser} from '../models/User';
import {TransferHttp} from '../../modules/transfer-http/transfer-http';
import {Observable} from 'rxjs/Observable';
import {Subject}    from 'rxjs/Subject';
@Injectable()
export class UserService {
  constructor(private transferHttp: TransferHttp, // Use only for GETS that you want re-used between Server render -> Client render
              private http: Http, // Use for everything else
              @Inject(ORIGIN_URL) private baseUrl: string) {
  }

  processTags(tags, tagsmain) {
    var temp = [];
    for (var i in tagsmain) {
      var found = false;
      for (var j in tags) {
        if (tagsmain[i].value == tags[j]) {
          found = true;
          temp.push({"value": tagsmain[i].value, checked: true, index: i, editable: tagsmain[i].editable});
          break;
        }
      }
      if (!found) {
        temp.push({"value": tagsmain[i].value, checked: false, index: i, editable: tagsmain[i].editable});
      }
    }
    return temp;
  }

  processResult(result: any, tagsmain: any) {
    var list = [];

    for (var i in result) {
      var obj = <any>{};
      try {
        if (typeof result[i] == "string") {
          obj = result[i];
        } else {
          obj = JSON.parse(result[i].name)
        }
      } catch (error) {
        obj = {};
      }
      var temp = Object.assign({
        id: result[i].id,
        index: i,
      }, obj);
      temp.tagviews = this.processTags(temp.tags, tagsmain);
      list.push(temp);
    }
    return list;
  }


  getTags() {
    return this.transferHttp.get(`${this.baseUrl}/api/tags`);
  }

  private contact = new Subject<any>();
  private tagers: any;
  setData(data, tags) {
    this.tagers = tags;
    this.contact.next(data);
  }

  getContactTags() {
    return this.tagers;
  }

  getData(): Observable<any> {
    return this.contact.asObservable();
  }

  getUser(user: IUser): Observable<IUser> {
    return this.transferHttp.get(`${this.baseUrl}/api/users/` + user.id);
  }

  deleteUser(user: IUser): Observable<any> {
    return this.http.delete(`${this.baseUrl}/api/users/` + user.id);
  }

  updateUser(newUserName): Observable<any> {
    var temp = Object.assign({},newUserName);
    delete temp.tagviews;
    delete temp.showtags;
    delete temp.index;
    return this.http.put(`${this.baseUrl}/api/users/` + temp.id, {id: temp.id,name: JSON.stringify(temp)});
  }

  addUser(newUserName): Observable<any> {
    return this.http.post(`${this.baseUrl}/api/users`, {name: JSON.stringify(newUserName)})
  }

  addTag(newTagName): Observable<any> {
    return this.http.post(`${this.baseUrl}/api/tags`, {name: newTagName})
  }
  getUsers(): Observable<IUser[]> {
    // ** TransferHttp example / concept **
    //    - Here we make an Http call on the server, save the result on the window object and pass it down with the SSR,
    //      The Client then re-uses this Http result instead of hitting the server again!

    //  NOTE : transferHttp also automatically does .map(res => res.json()) for you, so no need for these calls
    return this.transferHttp.get(`${this.baseUrl}/api/users`);
  }
}
