import { Pipe, PipeTransform } from '@angular/core';
import {isUndefined} from "util";

@Pipe({name: 'searchPipe'})
export class SearchPipe implements PipeTransform {
  // Transform is the new "return function(value, args)" in Angular 1.x
  transform(values, filter: any, tags: any) {
    if(filter && filter[0] == '#'){
      if(filter[1] == undefined){
        return values;
      }else {
        var temp = filter.substr(1, filter.length);
        return values.filter((item) =>{
          for(var i in item.tags){
           if((item.tags[i].toLowerCase().indexOf(temp.toLowerCase())) !== -1){
             return true;
           }
          }
        });
      }
    }else{
      // ES6 array destructuring
      if(isUndefined(filter) || filter == ''){
        return values;
      }else{
        return values.filter((item) =>
          ((item.name.toLowerCase().indexOf(filter) !== -1)
          ||
          item.company.toLowerCase().indexOf(filter) !== -1));
      }
    }
  }
}


/*
 Copyright 2017 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license
 */
