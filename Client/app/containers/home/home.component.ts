﻿import { Component, OnInit, Inject } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
declare const FB: any;
@Component({
    selector: 'app-home',
    templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

    title: string = 'Contacts';
    user: any = {};
    // Use "constructor"s only for dependency injection
    constructor(public translate: TranslateService) {
        FB.init({
            appId: '486963664742212',
            cookie: false,  // enable cookies to allow the server to access
            // the session
            xfbml: true,  // parse social plugins on this page
            version: 'v2.5' // use graph api version 2.5
        });
    }
    onFacebookLoginClick() {
        FB.login();
    }

    statusChangeCallback(resp) {
        var that = this;
        if (resp.status === 'connected') {
            // connect here with your server for facebook login by passing access token given by facebook
            if (FB) {
                FB.api('/me', 'get', { access_token: resp.authResponse.accessToken, fields: 'id,name,gender' }, function (response) {
                    that.user = response;
                });
            }
        } else if (resp.status === 'not_authorized') {

        } else {

        }
    };
    // Here you want to handle anything with @Input()'s @Output()'s
    // Data retrieval / etc - this is when the Component is "ready" and wired up
    ngOnInit() {
        FB.getLoginStatus(response => {
            this.statusChangeCallback(response);
        });
    }

    public setLanguage(lang) {
        this.translate.use(lang);
    }
    
}
