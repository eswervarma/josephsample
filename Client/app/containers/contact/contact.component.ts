import {Component, ChangeDetectionStrategy, ViewEncapsulation, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params, Data} from '@angular/router';
import {UserService} from '../../shared/user.service';
@Component({
  changeDetection: ChangeDetectionStrategy.Default,
  encapsulation: ViewEncapsulation.Emulated,
  selector: 'contact',
  templateUrl: './contact.component.html'
})
export class ContactComponent {
  public data = <any> {};
  private sub = <any>{};
  private characters = <any>{};

  constructor(private userService: UserService, private route: ActivatedRoute,
              private router: Router) {
    this.universalInit();
  }

  universalInit() {
    this.userService.getUsers()
      .subscribe(data => {
        console.log(data);
        this.characters = this.userService.processResult(data,this.userService.getContactTags());
        // Subscribe to route params
        this.sub = this.route.params.subscribe(params => {
          let id = params['id'];
          if (id && this.characters[id]) {
            this.data = this.characters[id];
          }
          this.userService.getData().subscribe(result => {
            if (Object.keys(data).length == 0) {
              this.data = this.characters[id];
            } else {
              this.data = result;
            }
          });
        });
      });
  }
}
