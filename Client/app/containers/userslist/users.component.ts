﻿import {
  Component, OnInit,
  // animation imports
  trigger, state, style, transition, animate, Inject
} from '@angular/core';
import {IUser} from '../../models/User';
import {UserService} from '../../shared/user.service';
declare const FB: any;
declare const PUBNUB: any;
@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  animations: [
    // Animation example
    // Triggered in the ngFor with [@flyInOut]
    trigger('flyInOut', [
      state('in', style({transform: 'translateY(0)'})),
      transition('void => *', [
        style({transform: 'translateY(-100%)'}),
        animate(1000)
      ]),
      transition('* => void', [
        animate(1000, style({transform: 'translateY(100%)'}))
      ])
    ])
  ]
})
export class UsersListComponent implements OnInit {
  users: IUser[];
  pubnub: any;
  selectedUser: IUser;
  data: any = {};
  searchtext: any;
  tags: any = {};
  characters: any;
  tagsFilterApplied: boolean = false;

  constructor(private userService: UserService) {

    // we need the data synchronously for the client to set the server response
    // we create another method so we have more control for testing
  }

  onSelect(user: any) {
    this.selectedUser = user;
  }
  handleChange(value, explicit) {
    if (value && value[0] == '#') {
      this.tagsFilterApplied = true;
    } else {
      this.tagsFilterApplied = false;
    }
    if (explicit) this.tagsFilterApplied = false;
    this.searchtext = value;
  }

  showTags(character, i, val) {
    this.characters[i].showtags = val;
  }

  updateTag(value) {
    var temp = value.tag;
    var found = false;
    for (var i in this.tags) {
      if (this.tags[i].value == temp) {
        found = true;
        break;
      }
    }
    if (!found) {
      this.pubnub = PUBNUB.init({
        publish_key: 'demo',
        subscribe_key: 'demo',
        uuid: temp
      });

      // This function updates the table by calling here_now.
      var updateTable = function (counter,that) {
        that.pubnub.here_now({
          channel: 'tags_key_exchange',
          state: true,
          callback: function (m) {
            var foundSelf = false;
            var taglist = [];
            for (var i = 0; i < m.uuids.length; i++) {
              var state = m.uuids[i].state;
              if (state.tag) {
                let found = false;
                for (var j = 0; j < that.tags.length; j++) {
                  if (that.tags[j].value == state.tag.value) {
                    found = true;
                  }
                }
                if (!found)
                  taglist.push(state.tag);
              }
              if (state.tag.value === temp) {
                foundSelf = true;
                that.tags = that.tags.concat(taglist);
                for(var k in that.characters){
                  that.characters[k].tagviews = that.userService.processTags(that.characters[k].tags,that.tags);
                }
              }
            }
            if (!foundSelf && counter <100) {
              // If the results of our here_now call
              // doesn't include ourself, try again.
              updateTable(counter++,that);
            }
          }
        });
      };
      this.pubnub.subscribe({
        channel: 'tags_key_exchange',
        callback: function (m) {
          console.log(m);
        },
        state: {
          tag: {value:temp}
        },
        presence: updateTable.bind(0,this,this),
        heartbeat: 60
      });
      this.addTag({value:temp});
      this.tags.push({value:temp});
      for(var i in this.characters){
        this.characters[i].tagviews = this.userService.processTags(this.characters[i].tags,this.tags);
      }
    }
  }

  setContactData(data,tags) {
    this.userService.setData(data,tags);
  }

  addTag(addTagName) {
    this.userService.addTag(addTagName).subscribe(result => {
      console.log('Post user result: ', result);
      if (result.ok) {
        // this.tags.push(addTagName);
      }
    }, error => {
      console.log(`There was an issue. ${error._body}.`);
    });
  }
  deleteTags(index,tag,checked){
    for (var i in this.tags) {
      if (this.tags[i].value == tag) {
        this.tags.splice(parseInt(i),1);
        break;
      }
    }
    for(var i in this.characters){
      for (var j in this.characters[i].tags) {
        if (this.characters[i].tags[j] == tag) {
          this.characters[i].tags.splice(parseInt(j),1);
          break;
        }
      }
      this.characters[i].tagviews = this.userService.processTags(this.characters[i].tags,this.tags);
      this.userService.updateUser(this.characters[i]).subscribe(result => {
        console.log('Post user result: ', result);
      }, error => {
        console.log(`There was an issue. ${error._body}.`);
      });
    }
  }
  updateTagValue(tag,oldtag){
    for (var i in this.tags) {
      if (this.tags[i].value == oldtag) {
        this.tags[i].editable = false;
        this.tags[i].value = tag.tager;
        break;
      }
    }
    for(var i in this.characters){
      for (var j in this.characters[i].tags) {
        if (this.characters[i].tags[j] == oldtag) {
          this.characters[i].tags[j] = tag.tager;
          break;
        }
      }
      this.characters[i].tagviews = this.userService.processTags(this.characters[i].tags,this.tags);
      this.userService.updateUser(this.characters[i]).subscribe(result => {
        console.log('Post user result: ', result);
      }, error => {
        console.log(`There was an issue. ${error._body}.`);
      });
    }
  }
  EditTags(cindex,tag,checked){
    for (var i in this.tags) {
      if (this.tags[i].value == tag) {
        this.tags[i].editable = true;
        break;
      }
    }
    for(var i in this.characters){
      this.characters[i].tagviews = this.userService.processTags(this.characters[i].tags,this.tags);
    }
  }
  updateTags(cindex, index, tag, checked) {
    var found = false;
    if (this.characters[cindex].tags == undefined) {
      this.characters[cindex].tags = [];
    }
    for (var i in this.characters[cindex].tags) {
      if (this.characters[cindex].tags[i] == tag) {
        found = true;
        if (checked == false) {
          this.characters[cindex].tags.splice(parseInt(i), 1);
        }
      }
    }
    if (checked && !found) {
      this.characters[cindex].tags.push(tag);
    }
    this.userService.updateUser(this.characters[cindex]).subscribe(result => {
      console.log('Post user result: ', result);
    }, error => {
      console.log(`There was an issue. ${error._body}.`);
    });
    this.characters[cindex].tagviews = this.userService.processTags(this.characters[cindex].tags,this.tags);
  }

  statusChangeCallback(resp) {
    if (resp.status === 'connected') {
      // connect here with your server for facebook login by passing access token given by facebook
      if (FB) {
        FB.api('/me', 'get', {
          access_token: resp.authResponse.accessToken,
          fields: 'id,name,gender'
        }, function (response) {
          this.data = response;
        });
      }
    } else if (resp.status === 'not_authorized') {
      this.data.error = "Not Authorized"
    } else {
      this.data.error = "Not Authorized"
    }
  };

  ngOnInit() {
    this.pubnub = PUBNUB.init({
      publish_key: 'demo',
      subscribe_key: 'demo',
      uuid: "change"
    });
    this.tags = [{"value":'CONSULTANT'},
      {"value":'MENTOR'},
      {"value":'SERVICE PROVIDER'},
      {"value":'TEAM MEMBER'}];
    FB.getLoginStatus(response => {
      this.statusChangeCallback(response);
    });
    this.userService.getUsers().subscribe(result => {
      this.characters = this.userService.processResult(result,this.tags)
    });
    this.userService.getTags().subscribe(result => {
      this.tags = this.tags.concat(result);
    });


  }
}
